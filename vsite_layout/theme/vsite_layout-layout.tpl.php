<div id="scholarlayout-container" class="pad">
  <?php print $vsite_layout_header_main; ?>
  <?php print $vsite_layout_header_left; ?>
  <?php print $vsite_layout_header_right; ?>
  <?php print $vsite_layout_navbar; ?>
  <?php print $vsite_layout_left; ?>
  <?php print $vsite_layout_content; ?>
  <?php print $vsite_layout_right; ?>
  <?php print $vsite_layout_footer; ?>
</div>